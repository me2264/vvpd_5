"""Арапов Александр КИ22-172б программа для записи работников фирмы"""
import csv
import pathlib


def check_file():
    """
    Проверяет существование файла Работнички.csv

    :return: возвращает True или False
    """
    path = pathlib.Path('работнички.csv')
    return path.exists()


def create_dict():
    """
    Функция для создания файла если его нет у пользователя

    Если файла нету, то создается и прописываются заголовки описанные в переменной
    fields. Если файл есть то проверяется пустой он, или нет. Пустой - снова записываются заголовки,
    не пустой - файл считывается. Все что можно записать - записывается в def_dict

    :return: список def_dict с работниками
    """
    def_dict = {}
    fields = ['Имя', 'Фамилия', 'Возраст', 'Должность', 'Оклад', 'Отдел']
    if not check_file():
        with open('работнички.csv', 'a+', encoding='utf-8', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fields, delimiter=';')
            writer.writeheader()
            def_dict[0] = fields
    else:
        with open('работнички.csv', 'a+', encoding='utf-8', newline='') as f:
            f.seek(0)
            s = f.readlines()  # переменная для проверки пустоты файла
            if len(s) == 0:
                writer = csv.DictWriter(f, fieldnames=fields, delimiter=';')
                writer.writeheader()
                def_dict[0] = fields
            else:
                f.seek(0)
                reader = csv.reader(f, delimiter=';')
                for j, value in enumerate(reader):
                    def_dict[j] = value
    return def_dict


def check_values(a):
    """
    Вспомогательная функция для прроверки введенных возрастов и окладов

    Преобразует строку в число, если строка является целочисленным числом

    :param a: строка которую нужно проверить
    :return: True or False для чилса
    :raise: ValueError

    example:

    1.
    >? '4t'
    >> False

    2.
    >? '567'
    >> 567
    """
    try:
        int(a)
    except ValueError as e:
        return False
    else:
        return True


def show_table(a):
    """
    Показывает данные хранящиеся в словаре
    :param a: словарь работников
    :return: выводит в консоль все данные словаря a

    example:

    >? {'Ваня': [.....], 'Петя': [.....]}
    >> 0 Ваня: [....]
    >> 1 Петя: [....]
    """
    for i, (k, v) in enumerate(a.items()):
        print(f'{k}: {v}')


def add_record(a):
    """
    Функция добавления нового сотрудника

    Принимает данные о сотруднкие, записывает их в файл, словарь не используется,
    функция вызывается с аргументом для простоты вызова ее в функции main()

    :param a: словрь сотрудников
    :return: измененный файл
    """
    print('Введите данные сотрудника')
    name = input('Имя: ')
    surname = input('Фамилия: ')
    age = input('Возраст: ')
    workside = input('Должность: ')
    salary = input('Оклад: ')
    workplace = input('Отдел: ')
    alfa_list = [name, surname, workside, workplace]
    alfa_list = [i.isalpha() for i in alfa_list]
    digit_list = [check_values(age), check_values(salary)]
    if all(alfa_list + digit_list):
        with open('работнички.csv', 'a', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([name, surname, age, workside, salary, workplace])
    else:
        print('Вы ввели некоректные данные')


def del_record(a):
    """
    Функция удаления сотрудника

    Удаляет сотрудника по его индексу, удалив его перезаписывает
    файл сдвигая индексы сотрудников

    :param a: словарь сотрудников
    :return: измененный словрь и переписанный файл

    :raise: ValueError
    """
    question = input('Введите номер сотрудника: ')
    if check_values(question) and int(question) in list(a.keys())[1:]:
        del a[int(question)]
        with open('работнички.csv', 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=';')
            for k, v in a.items():
                writer.writerow(list(v))
    else:
        print('Был введен не существующий сотрудник')


def sorted_age_salary(a, b):
    """
    сортировка сотрудников по возрасту и окладу

    Вывод в порядке возрастания сотрудников исходя из их возраста и зп
    Принимает два параметра, второй определяет какая часть функции будет задействована

    :param a: словарь сотрудников
    :param b: как будет производиться сортировка по возрасту или зп
    :return: выводится список работников по возрастанию
    """
    all_workers = dict(list(a.items())[1:])  # работники, не включая нулевого
    if b == 0:
        all_workers = dict(sorted(all_workers.items(), key=lambda x: int(x[1][2])))
        show_table(all_workers)
    elif b == 1:
        all_workers = dict(sorted(all_workers.items(), key=lambda x: int(x[1][4])))
        show_table(all_workers)


def sorted_work_place(a, b):
    """
    функция фильтрации работников по должности или отделу

    Выводит список сотруднков стоящие на определенной должности или
    работающие в отделе. Параметр b аналогичен как из функции sorted_age_salary

    :param a: словарь сотрудников
    :param b: как производиться фильтрация по должности или отделу
    :return: данные сотрудников которые работают в отделе или по работе
    """
    def_flag = True
    if b == 0:
        question = input('Введите должность по которой будет фильтрация: ')
        for (k, v) in a.items():
            if v[3] == question:
                da = True
                print(f'{k}:  {a[k]}')
        if not def_flag:
            print('Такой должности нет')
    elif b == 1:
        question = input('Введите отдел по которому будет фильтрация: ')
        for (k, v) in a.items():
            if v[5] == question:
                print(f'{k} - {a[k]}')
        if not def_flag:
            print('Такого отдела нет')


def main():
    """
    Главная функция в коде

    Запрашивает у пользователя команды для манипуляций с файлом или
    сотрудниками. Выводит меню доступных команд. Завершает программу.

    :return: то что попросил пользователь
    """
    commands = {
        'show': ('Вывести таблицу', show_table),
        'add': ('Добавить запись', add_record),
        'del': ('Удалить запись', del_record),
        'sort_age': ('Сортировка по возрасту', sorted_age_salary),
        'sort_sal': ('Сортировка по окладу', sorted_age_salary),
        'sort_work': ('Фильтрация по должности', sorted_work_place),
        'sort_place': ('Фильтрация по отделу', sorted_work_place),
    }
    for key, value in commands.items():
        print(f'{value[0]} - {key}')
    print('Выйтти из программы exit')
    print('-' * 25)
    flag = True
    while flag:
        workers_dict = create_dict()
        answer = input('Введите команду ')
        while (answer != 'exit') and (answer not in commands.keys()):
            answer = input('Неверная команда. Повторите ввод: ')
        if answer in list(commands.keys())[:3]:
            commands[answer][1](workers_dict)
        elif answer in ['sort_age', 'sort_work']:
            commands[answer][1](workers_dict, 0)
        elif answer in ['sort_sal', 'sort_place']:
            commands[answer][1](workers_dict, 1)
        elif answer == 'exit':
            flag = False


if __name__ == '__main__':
    main()
